package com.example.apollon.data

data class Response (
    val ok: Boolean,
    val result: Result
)